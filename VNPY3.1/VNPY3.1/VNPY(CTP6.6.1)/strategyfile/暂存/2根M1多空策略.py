from vnctptdType661 import *
import globalvar
import globalType
def checkinstrumentID(marketdata, strategyname):
    if  strategyname in globalvar.dict_strategyinstrument:
        if  str(marketdata.InstrumentID, encoding="utf-8") in globalvar.dict_strategyinstrument[strategyname]:
            return 0
        else:
            return 1
    else:
        return 1
#通过Python调用C++方法获取K线M1数据范例，Python可根据M1周期生成其他周期数据
def OnTick(marketdata,  strategyname):
    print('OnTick 2bar:'+str(marketdata.InstrumentID, encoding="utf-8"))
    print(__name__)
    global dict_strategyinstrument, dick_tick
    # 检查是否是本策略选中需要交易的合约编码，如果不在则返回，配置在策略文件名称一致的csv文件中，本策略是strategy1.csv，
    if checkinstrumentID(marketdata, strategyname):
        return

    #if globalvar.md.GetKline(marketdata.InstrumentID, 1).contents.Close > globalvar.md.GetKline(marketdata.InstrumentID, 1).contents.Open and \ globalvar.md.GetKline(marketdata.InstrumentID, 2).contents.Close > globalvar.md.GetKline(marketdata.InstrumentID, 2).contents.Open:
    if globalvar.data_kline_M1[0][globalvar.CLOSE] > globalvar.data_kline_M1[0][globalvar.OPEN] and globalvar.globalvar.data_kline_M1[1][globalvar.CLOSE] > globalvar.data_kline_M1[1][globalvar.OPEN]:
        print("买入" + str(marketdata.InstrumentID, encoding="utf-8"))
        # 策略池功能完善中
        # 用限价单 VN_OPT_LimitPrice 方式报单 。通常用涨停价限价单模拟市价，因为市价指令不支持所有的交易所
        exchangeid = globalvar.dict_exchange[str(marketdata.InstrumentID, encoding="utf-8")].split(',')[0]
        result = globalvar.td.InsertOrder(str(marketdata.InstrumentID, encoding="utf-8"), exchangeid, '0', '1',
                                          VN_OPT_LimitPrice, marketdata.LastPrice + 10, 1)
        result = result + globalvar.td.InsertOrder(str(marketdata.InstrumentID, encoding="utf-8"), exchangeid,
                                                   '0', '0', VN_OPT_LimitPrice, marketdata.LastPrice + 10, 1)
        if result == 0:
            print('报单成功')
        else:
            print('报单失败')
    elif globalvar.data_kline_M1[0][globalvar.CLOSE] < globalvar.data_kline_M1[0][globalvar.OPEN] and globalvar.globalvar.data_kline_M1[1][globalvar.CLOSE] < globalvar.data_kline_M1[1][globalvar.OPEN]:
        # 用限价单 VN_OPT_LimitPrice 方式报单 。通常用涨停价限价单模拟市价，因为市价指令不支持所有的交易所
        exchangeid = globalvar.dict_exchange[str(marketdata.InstrumentID, encoding="utf-8")].split(',')[0]
        result = globalvar.td.InsertOrder(str(marketdata.InstrumentID, encoding="utf-8"), exchangeid, '1', '1',
                                          VN_OPT_LimitPrice, marketdata.LastPrice - 10, 1)
        result = result + globalvar.td.InsertOrder(str(marketdata.InstrumentID, encoding="utf-8"), exchangeid,
                                                   '1', '0', VN_OPT_LimitPrice, marketdata.LastPrice - 10, 1)
        if result == 0:
            print('报单成功')
        else:
            print('报单失败')

    '''
    方式1（推荐）：
    #本方式数据，先从服务器获取数据，再根据本地TICK数据自动生成，
    #断网重连会从新从服务器获取K线数据，是否从服务器获取初始的数据是可以选择的，具体在VNTrader “主菜单->K线数据来源” 选择
    #本方式K线数据是存在Python的变量里，定义在 globalvar.py ，globalvar.py存储的变量是共享的全局变量，方式各个Python文件调用
    #只有 data_kline_M1  (M1周期数据)是从服务器获取或实时生成
    # 其他周期K线数据是通过Python的Pandas本地合成
    # 本地生成数据包括 data_kline_M3、data_kline_M5、data_kline_M10、data_kline_M15、data_kline_M30、data_kline_M60、data_kline_M120、data_kline_D1
    #字段分别为 ID,Data,Time,Open,Close,Low,High
    #调用方式如下
    print("data_kline_M1: " + str(data_kline_M1[0][globalvar.TRADINGDAY]))
    print("data_kline_M1: " + str(data_kline_M1[0][globalvar.KLINETIME]))
    print("data_kline_M1: " + str(data_kline_M1[0][globalvar.INSTRUMENT]))
    print("data_kline_M1: " + str(data_kline_M1[0][globalvar.OPEN]))
    print("data_kline_M1: " + str(data_kline_M1[0][globalvar.CLOSE]))
    print("data_kline_M1: " + str(data_kline_M1[0][globalvar.LOW]))
    print("data_kline_M1: " + str(data_kline_M1[0][globalvar.HIGH]))
    print("data_kline_M1: " + str(data_kline_M1[0][globalvar.VOL]))
    
    print("data_kline_M1: " + str(data_kline_M1[1][globalvar.TRADINGDAY]))
    print("data_kline_M1: " + str(data_kline_M1[1][globalvar.KLINETIME]))
    print("data_kline_M1: " + str(data_kline_M1[1][globalvar.INSTRUMENT]))
    print("data_kline_M1: " + str(data_kline_M1[1][globalvar.OPEN]))
    print("data_kline_M1: " + str(data_kline_M1[1][globalvar.CLOSE]))
    print("data_kline_M1: " + str(data_kline_M1[1][globalvar.LOW]))
    print("data_kline_M1: " + str(data_kline_M1[1][globalvar.HIGH]))
    print("data_kline_M1: " + str(data_kline_M1[1][globalvar.VOL]))
    方式2：
    #从开源DLL获得M1 K线数据
    print("M1 K线合约名称 ref(0): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 0).contents.InstrumentID, encoding="utf-8"))
    print("M1 K线结束时间 ref(0): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 0).contents.KlineTime))
    print("M1 K线 最高价 ref(0): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 0).contents.High))
    print("M1 K线 最低价 ref(0): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 0).contents.Low))
    print("M1 K线 收盘价 ref(0): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 0).contents.Close))
    print("M1 K线 成交量 ref(0): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 0).contents.Volume))
    print("M1 K线 交易日 ref(0): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 0).contents.TradingDay, encoding="utf-8"))

    print("M1 K线合约名称 ref(1): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 1).contents.InstrumentID, encoding="utf-8"))
    print("M1 K线结束时间 ref(1): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 1).contents.KlineTime))
    print("M1 K线 最高价 ref(1): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 1).contents.High))
    print("M1 K线 最低价 ref(1): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 1).contents.Low))
    print("M1 K线 收盘价 ref(1): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 1).contents.Close))
    print("M1 K线 成交量 ref(1): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 1).contents.Volume))
    print("M1 K线 交易日 ref(1): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 1).contents.TradingDay, encoding="utf-8"))
    '''