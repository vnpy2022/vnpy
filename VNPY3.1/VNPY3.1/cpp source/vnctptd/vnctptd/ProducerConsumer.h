#include "stdafx.h"
//#include <unistd.h>
#include <cstdlib>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <thread>



static const int kItemRepositorySize  = 10; // Item buffer size.
static const int kItemsToProduce  = 1000;   // How many items we plan to produce.


#ifndef ABCD_H
#define ABCD_H
typedef struct ItemRepository ItemRepository;
struct ItemRepository {
	int item_buffer[kItemRepositorySize]; // 产品缓冲区, 配合 read_position 和 write_position 模型环形队列.
	size_t read_position; // 消费者读取产品位置.
	size_t write_position; // 生产者写入产品位置.
	std::mutex mtx; // 互斥量,保护产品缓冲区
	std::condition_variable repo_not_full; // 条件变量, 指示产品缓冲区不为满.
	std::condition_variable repo_not_empty; // 条件变量, 指示产品缓冲区不为空.
} gItemRepository; // 产品库全局变量, 生产者和消费者操作该变量.

#endif // #ifndef ABCD_H







	  void ProduceItem(ItemRepository *ir, int item);
 

	  int ConsumeItem(ItemRepository *ir);
 

	  void ProducerTask();// 生产者任务
 

	  void ConsumerTask();// 消费者任务
 

	  void InitItemRepository(ItemRepository *ir);
 


